# planqk-cli-serve

This project is the baseline for the PlanQK CLI command `serve`.

It runs a PLANQK project and exposes it through a local web server, similarly to how PLANQK would run the code.
The local web server exposes the same HTTP endpoints to start a service execution, to check the status of running executions, to cancel executions, and to retrieve execution results.

## Usage

The container expects the project directory to be used mounted under the path `/workspace`.

Build the container image:

```bash
docker build -t planqk-cli-serve .
```

Once the container image has been built, start it with the following command:

```bash
docker run -it -e PORT=8000 -p 8000:8000 -v "$(pwd)/workspace:/workspace" planqk-cli-serve standalone
```

## Development

We use FastAPI, to run the service locally, you can use the following command:

```bash
uvicorn src.app:app --reload
```

By default, the `run_entrypoint_wrapper()` method tries to execute the supplied test directory `workspace`.
The implementation therein is the bare minimum a service needs to provide.
In this case, the logic simply prints/logs the input data and params and returns it as is wrapped in a dictionary.

You may attach to the container like the following:

```bash
docker run -it -v "$(pwd)/workspace:/workspace" --entrypoint bash planqk-cli-serve
```

## License

Apache-2.0 | Copyright 2023-present Kipu Quantum GmbH
