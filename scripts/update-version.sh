#!/usr/bin/env sh

version=${1:-0.0.0}

sed -i "s/^version = .*$/version = \"${version}\"/g" pyproject.toml

uv export --format requirements-txt --no-dev --no-emit-project > requirements.txt
uv export --format requirements-txt --only-dev --no-emit-project > requirements-dev.txt
