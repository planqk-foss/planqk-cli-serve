import logging
from typing import List, Dict, Any

from pydantic import BaseModel, Field

log = logging.getLogger(__name__)


class DataModel(BaseModel):
    values: List[float] = Field(title="Values",
                                description="List of values",
                                examples=[1, 5.2, 20, 7, 9.4])


class ResultModel(BaseModel):
    sum: float


def run(data: DataModel, params: Dict[str, Any] = None) -> ResultModel:
    result = sum(data.values)

    if params and params.get("round_off"):
        result = round(result)

    log.info(f"Returning result: {result}")

    return ResultModel(sum=result)
