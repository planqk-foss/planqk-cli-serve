ARG UV_VERSION=0.5
ARG PYTHON_VERSION=3.11
ARG BASE_LAYER=bookworm-slim

FROM ghcr.io/astral-sh/uv:$UV_VERSION-python$PYTHON_VERSION-$BASE_LAYER

ENV PORT=8081
ENV PYTHONUNBUFFERED=1
ENV WORKSPACE=/workspace

COPY ./src src
COPY .python-version /.python-version
COPY pyproject.toml /pyproject.toml
COPY uv.lock /uv.lock

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN uv venv
RUN uv sync --frozen

ENTRYPOINT ["/entrypoint.sh"]
CMD ["sh", "-c", "uvicorn src.app:app --reload --host 0.0.0.0 --port $PORT"]
