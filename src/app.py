import time
import uuid
from typing import List

from fastapi import FastAPI, Path, Query, BackgroundTasks, Response
from loguru import logger
from planqk.commons.json import any_to_json
from planqk.commons.logging import init_logging
from planqk.commons.parmeters import is_simple_type
from starlette.requests import Request

from src.helpers.date_formatter import format_timestamp
from src.job_executor import JobExecutor
from src.model.execution_status import ExecutionStatus
from src.model.health_check import HealthCheck
from src.model.job import Job
from src.model.parameters import Input
from src.model.parameters import Output

app = FastAPI(
    title="PLANQK Service (Development)",
    version="0.1",
)

init_logging()

job_executor = JobExecutor()


@app.get('/',
         tags=["Status API"],
         summary="Health checking endpoint")
def health_check() -> HealthCheck:
    return HealthCheck(status="Service is up and running")


@app.post('/',
          tags=["Service API"],
          summary="Asynchronous execution of the service",
          status_code=201)
async def create_job(request: Request, input_model: Input, background_tasks: BackgroundTasks) -> Job:
    raw_input = await request.json()
    job_id = str(uuid.uuid4())
    background_tasks.add_task(job_executor.create_job, job_id, raw_input)
    return Job(id=job_id, status=ExecutionStatus.PENDING, created_at=format_timestamp(time.time()), started_at=None, ended_at=None)


@app.get('/{id}',
         tags=["Service API"],
         summary="Check service execution status")
def get_job_status(job_id: str = Path(alias="id", description="The ID of a certain service execution")) -> Job:
    return job_executor.get_job_status(job_id)


@app.get('/{id}/result',
         tags=["Service API"],
         summary="Get the result of a service execution",
         response_model=Output)
def get_job_result(job_id: str = Path(alias="id", description="The ID of a certain service execution")):
    return_value = job_executor.get_job_result(job_id)
    logger.debug(f"Return type: {type(return_value)}")

    string_value = any_to_json(return_value)
    if string_value is None:
        return 0

    if is_simple_type(return_value):
        return Response(content=string_value, media_type="text/plain")
    else:
        return Response(content=string_value, media_type="application/json")


@app.get('/{id}/interim-results',
         tags=["Service API"],
         summary="Get the last or a list of interim results of a service execution")
def get_interim_results(
        job_id: str = Path(alias="id", description="The ID of a certain service execution"),
        last: bool = Query(
            False, description="Either true or false to show only the last or all interim results"
        )
) -> List[Output] | Output:
    return []


@app.put('/{id}/cancel',
         tags=["Service API"],
         summary="Cancel a service execution")
def cancel_job(job_id: str = Path(alias="id", description="The ID of a certain service execution")) -> Job:
    return job_executor.cancel_job(job_id)
