from pydantic import BaseModel


class Input(BaseModel):
    model_config = {
        "extra": 'allow'
    }


class Output(BaseModel):
    model_config = {
        "extra": 'allow'
    }
