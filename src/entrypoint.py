import traceback
from typing import Any, Dict, get_origin

from loguru import logger
from planqk.commons.entrypoint import run_entrypoint
from planqk.commons.json import any_to_json
from planqk.commons.parmeters import str_to_parameter_type
from planqk.commons.reflection import resolve_signature


def run_entrypoint_wrapper(entrypoint: str, raw_input: Dict[str, Any]) -> Any:
    try:
        entrypoint_signature = resolve_signature(entrypoint)

        parameters = {}

        for parameter in entrypoint_signature.parameters.values():
            parameter_name = parameter.name
            parameter_type = parameter.annotation

            # skip parameters without input
            if parameter_name not in raw_input:
                continue

            origin = get_origin(parameter_type)
            if origin:
                parameter_type = origin

            file_content = any_to_json(raw_input[parameter_name])

            parameter_value = str_to_parameter_type(file_content, parameter_type)

            parameters[parameter_name] = parameter_value

        return run_entrypoint(entrypoint, parameters)
    except Exception as e:
        logger.error(f"{e}")
        traceback.print_exc()
        raise e
