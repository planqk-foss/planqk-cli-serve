import os
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, Any

from fastapi import HTTPException

from src.entrypoint import run_entrypoint_wrapper
from src.job_state import JobState
from src.model.job import Job


class JobExecutor:
    def __init__(self):
        self.jobs: Dict[str, JobState] = {}
        self.executor = ThreadPoolExecutor(max_workers=3)

    def create_job(self, job_id: str, raw_input: Dict[str, Any]) -> None:
        entrypoint = os.environ.get("ENTRYPOINT", "workspace.src.program:run")

        future = self.executor.submit(run_entrypoint_wrapper, entrypoint, raw_input)

        job = JobState(job_id, future)
        self.jobs[job_id] = job

    def get_job_status(self, job_id: str) -> Job:
        job = self.jobs.get(job_id)
        if job is None:
            raise HTTPException(status_code=404, detail="Not found")

        return job.get_status()

    def get_job_result(self, job_id: str):
        job = self.jobs.get(job_id)
        if job is None:
            raise HTTPException(status_code=404, detail="Not found")

        result = job.get_result()
        if result is None:
            raise HTTPException(status_code=404, detail="Not found")

        return result

    def cancel_job(self, job_id: str):
        job = self.jobs.get(job_id)
        if job is None:
            raise HTTPException(status_code=404, detail="Not found")

        job.cancel()
        return job.get_status()
