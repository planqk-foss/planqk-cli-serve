import pytest

from src.entrypoint import run_entrypoint_wrapper


def run_valid(**kwargs):
    return {"result": True}


def run_exception(**kwargs):
    raise Exception()


def test_execute_user_code_with_valid_response():
    entry_point = "test.test_entrypoint:run_valid"
    response = run_entrypoint_wrapper(entry_point, {})

    assert response == {"result": True}


def test_execute_user_code_with_exception():
    entry_point = "test.test_entrypoint:run_exception"

    with pytest.raises(Exception):
        run_entrypoint_wrapper(entry_point, {})


def test_execute_user_code_with_invalid_entry_point():
    entry_point = "test.test_entrypoint:invalid_entry_point"

    with pytest.raises(Exception):
        run_entrypoint_wrapper(entry_point, {})


def test_execute_user_code_with_null_entry_point():
    with pytest.raises(Exception):
        run_entrypoint_wrapper(None, {})
